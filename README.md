Edit the ingress:
```
        - --kubernetes.enablepasstlscert=true
```

Creating the certificate (edit openssl.cnf first - change passtlstest001.cern.ch):
```
openssl genrsa -out tls.key 3072 -config openssl.cnf; openssl req -new -x509 -key tls.key -sha256 -out tls.crt -days 730 -config openssl.cnf

kubectl create secret tls go-headers-secret --key=tls.key --cert=tls.crt
```

Deployment (see example file):
```
kubectl create -f deployment.yaml
```

The sample Go app lists all headers passed to the backend (user + the traefik
added ones):
```
echo | openssl s_client -showcerts -servername passtlstest001.cern.ch -connect passtlstest001.cern.ch:443 2>/dev/null  > ca.crt

curl --cacert ca.crt https://passtlstest001.cern.ch
GET / HTTP/1.1 
Header field "Accept", Value ["*/*"]
Header field "X-Forwarded-For", Value ["128.141.193.78"]
Header field "X-Forwarded-Server", Value ["rocha-passtls-001-4wkatfbmsqoh-minion-0.cern.ch"]
Header field "User-Agent", Value ["curl/7.61.0"]
Header field "X-Forwarded-Host", Value ["passtlstest001.cern.ch"]
Header field "X-Forwarded-Port", Value ["443"]
Header field "X-Forwarded-Proto", Value ["https"]
Header field "X-Real-Ip", Value ["128.141.193.78"]
Header field "Accept-Encoding", Value ["gzip"]
Host = "passtlstest001.cern.ch"
RemoteAddr= "10.100.50.1:33814"

```
